# Supported tags and respective Dockerfile links

- [`8.5.23`](https://bitbucket.org/mikolajr/docker-tomcat/src/master/8/5/23/Dockerfile)
- [`8.5.24`](https://bitbucket.org/mikolajr/docker-tomcat/src/master/8/5/24/Dockerfile)
- [`8.5.20`](https://bitbucket.org/mikolajr/docker-tomcat/src/master/8/5/20/Dockerfile)
- [`8.0.38`](https://bitbucket.org/mikolajr/docker-tomcat/src/master/8/0/38/Dockerfile)
- [`8.0.48`](https://bitbucket.org/mikolajr/docker-tomcat/src/master/8/0/48/Dockerfile)
- [`7.0.55`](https://bitbucket.org/mikolajr/docker-tomcat/src/master/7/0/55/Dockerfile)
- [`7.0.42`](https://bitbucket.org/mikolajr/docker-tomcat/src/master/7/0/42/Dockerfile)

For more information about this image and its history, please see [its source repository](https://bitbucket.org/mikolajr/docker-tomcat). 
This image is automagically built and updated via pull requests.
Only the most recent version of Tomcat is being maintained.

# What is Tomcat?

[Apache Tomcat](https://en.wikipedia.org/wiki/Apache_Tomcat), often referred to as Tomcat, is an open-source web server developed by the Apache Software Foundation (ASF). Tomcat implements several Java EE specifications including Java Servlet, JavaServer Pages (JSP), Java EL, and WebSocket, and provides a "pure Java" HTTP web server environment in which Java code can run.

# Why another Tomcat image?

This image was prepared with simplicity and extendibility in mind. 
It's optimized for Linux like environments, does not contain unnecessary files (e.g. Windows startup scripts, example webapps, documentation).

General knowledge how to use Docker containers is recommended.

In order to use it one has to provide its own webapps directory with applications to run (either as war files or exploded directories) to be mounted as volume mount ([mount a host directory as a data volume](https://docs.docker.com/engine/tutorials/dockervolumes/#/mount-a-host-directory-as-a-data-volume)). 
Same applies for directory with logfiles.

Image exposes its HTTP and AJP ports.

# How to use this image

Let's assume that `/opt/webapps` is your directory with war files you want to run and `8080` is the port number you want to use to access your applications.

To run your webapps and remove the container after shutdown just type:

```console
docker run --rm -it -p 8080:8080 -v /opt/webapps:/usr/local/tomcat/webapps mikolajr/docker-tomcat:8.5.20
```

# Overriding defaults

This image provides a way to override default settings. 

It is possible to provide set of files which will be copied over Tomcat installation prior server start. This allows to e.g. copy JDBC drivers to `lib` 
directory, provide own `bin/setenv.sh` file, etc. Note: as for `setenv.sh`, one can rather pass environment variables like CATALINA_OPTS via docker `-e` 
option.

One can specify UID of tomcat user (if none is set user root will be used). Once UID is specified (using TOMCAT_UID variable) all contents under 
/usr/local/tomcat will be chowned to that uid, tomcat process will be ran as that uid as well.

Same applies to setting GID of tomcat user. To alter GID use TOMCAT_GID variable.

Altering UID/GID is helpful when working with host directories mounted by containers. It eliminates problems with file ownership.

1. Prepare empty directory, e.g. `/opt/skeleton`
2. Populate it with directory structure and files you want to copy to default Tomcat installation, e.g. put `mysql-jdbc.jar` in `lib` directory.
3. Mount `/opt/skeleton` as host directory mount when running a docker image.
4. Specify container-side location of that skeleton directory using TOMCAT_HOST_OVERRIDE environment variable (in this example we will mount skeleton directory to `/mnt` inside container).
5. We want to use 1090 as UID of tomcat user, so we will pass `-e TOMCAT_UID=1090` to docker.
6. We want to use 1099 as GID of tomcat user, so we will pass `-e TOMCAT_GID=1099` to docker.

Enhanced previous example:

```console
docker run --rm -it -p 8080:8080 -v /opt/webapps:/usr/local/tomcat/webapps -v /opt/skeleton:/mnt -e TOMCAT_HOST_OVERRIDE=/mnt -e TOMCAT_UID=1090 -e TOMCAT_GID=1099 mikolajr/docker-tomcat:8.5.20
```


# Image details

$JAVA_HOME: `/usr/lib/jvm/java-1.8-openjdk/jre`

Installation directory (aka CATALINA_HOME): `/usr/local/tomcat`

Webapps: `${CATALINA_HOME}/webapps`

Logs: `${CATALINA_HOME}/logs`

HTTP port: `8080`

HTTPS port: `8443`

AJP port `8009`

# Contributing

You are invited to contribute new features, fixes, or updates, large or small.
